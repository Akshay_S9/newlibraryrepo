from django.db import models
import datetime
from django.utils import timezone
# Create your models here.
class Question(models.Model):
	def __str__(self):
		return self.question_data

	def was_published_recently(self):
		return self.date_of_submission >= timezone.now() - datetime.timedelta(days=1)

	question_data = models.CharField(max_length=200)
	date_of_submission = models.DateTimeField('date Published')

class Choice(models.Model):
	def __str__(self):
		return self.choice_data
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	choice_data = models.CharField(max_length=200)
	votes = models.IntegerField(default=0)

